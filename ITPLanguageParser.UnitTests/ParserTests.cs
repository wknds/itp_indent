using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using ITPLanguageParser;

namespace ITPLanguageParser.UnitTests
{
    [TestClass]
    public class ParserTests
    {
        [TestMethod]
        public void TestBeginEndScope()
        {
            //Assert.ThrowsException<ParseNoMarkerFoundException>(() => new Parser("Blabla").Parse());
            //Assert.ThrowsException<ParseITPExpressionException>(() => new Parser("#BEGINBlabla").Parse());
            //Assert.ThrowsException<ParseNoEnclosingMarkerFoundException>(() => new Parser("#BEGIN Babla\n").Parse());
            //Assert.ThrowsException<ParseITPExpressionException>(() => new Parser("#BEGIN# Babla\n#").Parse());

            var str = "#BEGIN Babla\n## Hi\nEND#";
            Assert.AreEqual(new Parser(str).Parse(), str.Length);
            str = "#BEGIN Babla\ndwhauihui\ndwhauihui\n## END#";
            Assert.AreEqual(new Parser(str).Parse(), str.Length);
            str = "#BEGIN Babla\n     " +
                "dwhauihui     \n" +
                "dwhauihui\n" +
                "#\n #Heee\n# #END#";
            Assert.AreEqual(new Parser(str).Parse(), str.Length);

            var t = System.IO.File.ReadAllText("./Testfiles/Test.txt");
            Assert.AreEqual(new Parser(t).Parse(), t.Length);
            t = System.IO.File.ReadAllText("./Testfiles/Test2.txt");
            Assert.AreEqual(new Parser(t).Parse(), t.Length);
        }
    }
}
