//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     ANTLR Version: 4.9
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from /home/yaw/git/ITPLanguageParser/antlr4-grammar/ItpParser.g4 by ANTLR 4.9

// Unreachable code detected
#pragma warning disable 0162
// The variable '...' is assigned but its value is never used
#pragma warning disable 0219
// Missing XML comment for publicly visible type or member '...'
#pragma warning disable 1591
// Ambiguous reference in cref attribute
#pragma warning disable 419

namespace Antlr4.Generated {

using Antlr4.Runtime.Misc;
using IErrorNode = Antlr4.Runtime.Tree.IErrorNode;
using ITerminalNode = Antlr4.Runtime.Tree.ITerminalNode;
using IToken = Antlr4.Runtime.IToken;
using ParserRuleContext = Antlr4.Runtime.ParserRuleContext;

/// <summary>
/// This class provides an empty implementation of <see cref="IItpParserListener"/>,
/// which can be extended to create a listener which only needs to handle a subset
/// of the available methods.
/// </summary>
[System.CodeDom.Compiler.GeneratedCode("ANTLR", "4.9")]
[System.Diagnostics.DebuggerNonUserCode]
[System.CLSCompliant(false)]
public partial class ItpParserBaseListener : IItpParserListener {
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.start"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterStart([NotNull] ItpParser.StartContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.start"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitStart([NotNull] ItpParser.StartContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.block"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterBlock([NotNull] ItpParser.BlockContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.block"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitBlock([NotNull] ItpParser.BlockContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.instrMode"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterInstrMode([NotNull] ItpParser.InstrModeContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.instrMode"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitInstrMode([NotNull] ItpParser.InstrModeContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.include"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterInclude([NotNull] ItpParser.IncludeContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.include"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitInclude([NotNull] ItpParser.IncludeContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.txtMode"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterTxtMode([NotNull] ItpParser.TxtModeContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.txtMode"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitTxtMode([NotNull] ItpParser.TxtModeContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.stat"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterStat([NotNull] ItpParser.StatContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.stat"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitStat([NotNull] ItpParser.StatContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.doStat"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterDoStat([NotNull] ItpParser.DoStatContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.doStat"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitDoStat([NotNull] ItpParser.DoStatContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.decl"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterDecl([NotNull] ItpParser.DeclContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.decl"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitDecl([NotNull] ItpParser.DeclContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.variableDecl"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterVariableDecl([NotNull] ItpParser.VariableDeclContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.variableDecl"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitVariableDecl([NotNull] ItpParser.VariableDeclContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.type"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterType([NotNull] ItpParser.TypeContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.type"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitType([NotNull] ItpParser.TypeContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.funcDecl"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterFuncDecl([NotNull] ItpParser.FuncDeclContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.funcDecl"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitFuncDecl([NotNull] ItpParser.FuncDeclContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.declParams"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterDeclParams([NotNull] ItpParser.DeclParamsContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.declParams"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitDeclParams([NotNull] ItpParser.DeclParamsContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.declParam"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterDeclParam([NotNull] ItpParser.DeclParamContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.declParam"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitDeclParam([NotNull] ItpParser.DeclParamContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.typeStructure"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterTypeStructure([NotNull] ItpParser.TypeStructureContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.typeStructure"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitTypeStructure([NotNull] ItpParser.TypeStructureContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.assignment"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterAssignment([NotNull] ItpParser.AssignmentContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.assignment"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitAssignment([NotNull] ItpParser.AssignmentContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.funcCall"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterFuncCall([NotNull] ItpParser.FuncCallContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.funcCall"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitFuncCall([NotNull] ItpParser.FuncCallContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.exprList"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterExprList([NotNull] ItpParser.ExprListContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.exprList"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitExprList([NotNull] ItpParser.ExprListContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.expr"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterExpr([NotNull] ItpParser.ExprContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.expr"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitExpr([NotNull] ItpParser.ExprContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.variableName"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterVariableName([NotNull] ItpParser.VariableNameContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.variableName"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitVariableName([NotNull] ItpParser.VariableNameContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.ifStat"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterIfStat([NotNull] ItpParser.IfStatContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.ifStat"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitIfStat([NotNull] ItpParser.IfStatContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.condition"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterCondition([NotNull] ItpParser.ConditionContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.condition"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitCondition([NotNull] ItpParser.ConditionContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.condOp"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterCondOp([NotNull] ItpParser.CondOpContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.condOp"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitCondOp([NotNull] ItpParser.CondOpContext context) { }
	/// <summary>
	/// Enter a parse tree produced by <see cref="ItpParser.value"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void EnterValue([NotNull] ItpParser.ValueContext context) { }
	/// <summary>
	/// Exit a parse tree produced by <see cref="ItpParser.value"/>.
	/// <para>The default implementation does nothing.</para>
	/// </summary>
	/// <param name="context">The parse tree.</param>
	public virtual void ExitValue([NotNull] ItpParser.ValueContext context) { }

	/// <inheritdoc/>
	/// <remarks>The default implementation does nothing.</remarks>
	public virtual void EnterEveryRule([NotNull] ParserRuleContext context) { }
	/// <inheritdoc/>
	/// <remarks>The default implementation does nothing.</remarks>
	public virtual void ExitEveryRule([NotNull] ParserRuleContext context) { }
	/// <inheritdoc/>
	/// <remarks>The default implementation does nothing.</remarks>
	public virtual void VisitTerminal([NotNull] ITerminalNode node) { }
	/// <inheritdoc/>
	/// <remarks>The default implementation does nothing.</remarks>
	public virtual void VisitErrorNode([NotNull] IErrorNode node) { }
}
} // namespace Antlr4.Generated
