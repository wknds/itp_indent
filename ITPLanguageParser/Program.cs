﻿using System;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;
using Antlr4.Generated;
using System.Collections.Generic;

namespace ITPLanguageParser
{
    class Program
    {
        static void Main(string[] args)
        {
            String input = "#BEGIN ASSIGN text := \"Hello\" " + 
                "ASSIGN pi := 3.141" +
                "END#";
            Console.Write("Parse... ");  
            ICharStream stream = CharStreams.fromString(input);
            ITokenSource lexer = new ItpLexer(stream);
            ITokenStream tokens = new CommonTokenStream(lexer);
            ItpParser parser = new ItpParser(tokens);
            parser.BuildParseTree = true;

            IParseTree tree = parser.start();

            Console.WriteLine("[erfolgreich]");
            
            var walker = new ParseTreeWalker();   
            var coll = new VariableCollector();
            walker.Walk(coll, tree); 
            Console.Write("Verwendete Variabeln: ");
            foreach(var variable in coll.UsedVariables) {
                Console.Write(variable + "; ");
            }
            Console.WriteLine("");
        }
    }

    class VariableCollector : ItpParserBaseListener {
         public LinkedList<string> UsedVariables = new LinkedList<string>();

         public override void ExitVariableName(ItpParser.VariableNameContext ctx) {
            var name = ctx.ID(0).GetText();
            UsedVariables.AddLast(name);
         }
    }
}
